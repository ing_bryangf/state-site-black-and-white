<?php
/**
 * @file
 * black_and_white.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function black_and_white_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'The footer copyrights information';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'footer_copyrights';
  $fe_block_boxes->body = '<p>© 2015 Ruben Stom.</p>
';

  $export['footer_copyrights'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Several chunks of text with important information about somethin';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'footer_information_blocks';
  $fe_block_boxes->body = '<div id="blackandwhite-chunk">
<p class="blocks-title"><span>Black &amp; White</span></p>

<p>consequuntur magni dolores atione voluptatem sequi nesc ue porro quisquam est, qui d sum quia dolor sit amet,cons sum quia sum quia</p>
</div>

<div id="contact-chunk">
<p class="blocks-title"><span>Contact</span></p>

<p>www.loremipsumdolar.nl 182 AB Your Street Name e-mail@loremipsum.com +(121) 01 23 45 679</p>
</div>

<div id="tweet-chunk">
<p class="blocks-title"><span>Latest Tweet</span></p>

<p>“...aerat vluptatem. Ut eni et illi dolore magnam aliqua et aerat voluptatem. Utils eni tils &nbsp;et al is minima veniam.” 3 hours ago</p>
</div>
';

  $export['footer_information_blocks'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Homepage first white block';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'homepage_first_white_block';
  $fe_block_boxes->body = '<div id="homepage-first-block-title">Lorem ipsum dolor, consectuer adipiscing. Aenean commodo ligula eget dolor.</div>

<div id="homepage-first-block-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. .</div>
';

  $export['homepage_first_white_block'] = $fe_block_boxes;

  return $export;
}
