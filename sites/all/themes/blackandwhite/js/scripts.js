/**
 * @file
 * Attaches the behaviors for the ft_cart module.
 */

(function($) {
  Drupal.behaviors.blackandwhite = {
    attach: function(context, settings) {
      $(document).ready(function() {
        // Handler for .ready() called.
        blackandwhite_fix_slowgan_position();
      });
      
      // Declare here functions.
      
      /**
       * This function set the position of the slowgan into the
       * header.
       * @returns {undefined}
       */
      function blackandwhite_fix_slowgan_position() {
        var siteNameElement = $('#name-and-slogan').html();
        $( "#name-and-slogan" ).remove();
        $( siteNameElement ).appendTo( "#block-nice-menus-1" );
      }
    }
  };
})(jQuery);

